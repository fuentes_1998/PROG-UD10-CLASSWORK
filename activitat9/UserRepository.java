package com.prog.activitat9;

import com.prog.activitat8.User;

import java.util.ArrayList;

public interface UserRepository {

    boolean save(User user);

    User findById(int id);

    User getById(int id) throws UserNotFoundException;

    User findUserByEmail(String email);

    ArrayList<User> findUsersByEnterprise(int idEnterprise, int page);

    boolean dropUser(int id);
}
