package com.prog.activitat9;

import com.prog.activitat1.MySqlConnection;
import com.prog.activitat8.User;

import java.sql.*;
import java.util.ArrayList;

public class DbUserRepository implements UserRepository{

    private MySqlConnection mySqlConnection;

    public DbUserRepository() {

        this.mySqlConnection = new MySqlConnection("crm_db","user_java","password_java");
    }

    @Override
    public boolean save(User user) {

        if (user.getId() != null) {

            return this.update(user);
        }

        return this.insert(user);
    }

    @Override
    public User findById(int id) {

        String sql = "SELECT * FROM User WHERE id="+ id;

        Connection connection = mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            if (rs.next()) {

                return new User(
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("email"),
                        rs.getString("phoneNumber"),
                        rs.getString("password"),
                        rs.getString("locale"),
                        rs.getInt("idEnterprise"),
                        rs.getDate("birthday").toLocalDate()
                );
            }

            return null;

        }catch(SQLException e) {

            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User getById(int id) throws UserNotFoundException {

        User user = findById(id);

        if (user != null) {

            return user;
        }

        throw new UserNotFoundException();
    }

    @Override
    public User findUserByEmail(String email) {

        String sql = "SELECT * FROM User WHERE email="+ email;

        Connection connection = mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            if (rs.next()) {

                return new User(
                        rs.getInt("id"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("email"),
                        rs.getString("phoneNumber"),
                        rs.getString("password"),
                        rs.getString("roles"),
                        rs.getString("salt"),
                        rs.getTimestamp("lastLogin").toLocalDateTime(),
                        rs.getString("locale"),
                        rs.getInt("idEnterprise"),
                        rs.getDate("birthday").toLocalDate()
                );
            }

            return null;

        }catch(SQLException e) {

            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<User> findUsersByEnterprise(int idEnterprise, int page) {

        ArrayList<User> enterpriseUsers = new ArrayList<>();

        String sql = "SELECT * FROM User WHERE idEnterprise=" + idEnterprise + " LIMIT " + page;

        Connection connection = mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {

                int idUser = rs.getInt("id");

                enterpriseUsers.add(findById(idUser));
            }

            return enterpriseUsers;

        }catch(SQLException e) {

            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean dropUser(int id) {

        String sql = "DELETE FROM User WHERE id=" + id;

        Connection connection = mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);

            return true;

        } catch(SQLException e) {

            e.printStackTrace();
        }

        return false;
    }

    private boolean update(User user) {

        String sql = "UPDATE User SET ";

        sql+= "firstName='"+ user.getFirstName() + "', "+
                "lastName='"+ user.getLastName() + "', "+
                "email='"+ user.getEmail() + "', "+
                "phoneNumber='"+ user.getPhoneNumber() + "', "+
                "password='"+ user.getPassword() + "', "+
                "locale='"+ user.getLocale() + "', "+
                "idEnterprise='"+ user.getIdEnterprise() + "', "+
                "birthday='"+ user.getBirthday() + "' "+
                "WHERE id="+ user.getId();

        Connection connection = mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            return true;

        } catch(SQLException e) {

            e.printStackTrace();
            return false;
        }
    }

    private boolean insert(User user) {

        String sql = "INSERT INTO User (firstName,lastName,email,phoneNumber,password,createdOn,locale,idEnterprise,birthday) ";

        sql += "VALUES (" +
                "'"+ user.getFirstName() + "', " +
                "'"+ user.getLastName() + "', " +
                "'"+ user.getEmail() + "', " +
                "'"+ user.getPhoneNumber() + "', " +
                "'"+ user.getPassword() + "', " +
                "'"+ user.getCreatedOn() + "', " +
                "'"+ user.getLocale() + "', " +
                "'"+ user.getIdEnterprise() + "', " +
                "'"+ user.getBirthday() + "'" +
                ");";

        Connection connection = mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql,  Statement.RETURN_GENERATED_KEYS);
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {

                int idUser = generatedKeys.getInt(1);
                user.setId(idUser);
                return true;
            }

        } catch(SQLException e) {

            e.printStackTrace();

        }
        return false;
    }
}
