package com.prog.activitat9;

import com.prog.activitat8.User;

import java.time.LocalDate;

public class App {

    public static void main(String[] args) {

        User userToInsert = new User(
                "Jesucristo",
                "Garcia",
                "inthenight@hotmail.es",
                "617666666",
                "98hfejvdisb",
                "es_ES",
                2,
                LocalDate.parse("2000-12-11"));

        DbUserRepository dbUserRepository = new DbUserRepository();
        dbUserRepository.save(userToInsert);

        User user;

        try {

            user = dbUserRepository.getById(userToInsert.getId());

            System.out.println(user);

        }catch(UserNotFoundException e) {

            e.printStackTrace();
        }
    }
}
