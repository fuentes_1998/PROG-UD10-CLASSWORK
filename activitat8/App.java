package com.prog.activitat8;

import com.prog.activitat1.MySqlConnection;

import java.sql.SQLException;
import java.sql.Statement;

public class App {

    public static void main( String[] args ) {

        UserFactory userFactory = new UserFactory();

        User[] users = new User[5];

        for (int i = 0; i < users.length; i++) {

            users[i] = userFactory.create();

            saveUsers(users[i]);
        }
    }

    private static void saveUsers(User user) {

        MySqlConnection mySqlConnection = new MySqlConnection("crm_db","user_java","password_java");

        try {

            Statement statement = mySqlConnection.getConnection().createStatement();

            String sql = "INSERT INTO User (firstName,lastName,email,phoneNumber,password,createdOn,locale,idEnterprise,birthday) ";

            sql += "VALUES (" +
                    "'"+ user.getFirstName() + "', " +
                    "'"+ user.getLastName() + "', " +
                    "'"+ user.getEmail() + "', " +
                    "'"+ user.getPhoneNumber() + "', " +
                    "'"+ user.getPassword() + "', " +
                    "'"+ user.getCreatedOn() + "', " +
                    "'"+ user.getLocale() + "', " +
                    "'"+ user.getIdEnterprise() + "', " +
                    "'"+ user.getBirthday() + "'" +
                    ");";

            statement.executeUpdate(sql);

            System.out.println("Insertado");

        } catch(SQLException ex) {

            ex.printStackTrace();
        }
    }
}
