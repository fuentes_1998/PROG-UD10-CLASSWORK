package com.prog.activitat8;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class User {

    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String password;
    private String roles;
    private Boolean active;
    private String salt;
    private LocalDateTime createdOn;
    private LocalDateTime lastLogin;
    private String locale;
    private Integer idEnterprise;
    private LocalDate birthday;

    public User(String firstName, String lastName, String email, String phoneNumber, String password, String locale, Integer idEnterprise, LocalDate birthday) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.lastLogin = LocalDateTime.now();
        this.locale = locale;
        this.idEnterprise = idEnterprise;
        this.birthday = birthday;
        this.createdOn = LocalDateTime.now();
        this.active = true;
    }

    public User(Integer id, String firstName, String lastName, String email, String phoneNumber, String password, String roles, String salt, LocalDateTime lastLogin, String locale, Integer idEnterprise, LocalDate birthday) {

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.roles = roles;
        this.salt = salt;
        this.lastLogin = lastLogin;
        this.locale = locale;
        this.idEnterprise = idEnterprise;
        this.birthday = birthday;
        this.createdOn = LocalDateTime.now();
        this.active = true;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public String getRoles() {
        return roles;
    }

    public Boolean isActive() {
        return active;
    }

    public String getSalt() {
        return salt;
    }

    public String getCreatedOn() {
        return createdOn.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public String getLastLogin() {
        return lastLogin.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public String getLocale() {
        return locale;
    }

    public Integer getIdEnterprise() {
        return idEnterprise;
    }

    public String getBirthday() {
        return birthday.format(DateTimeFormatter.BASIC_ISO_DATE);
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public void setIdEnterprise(Integer idEnterprise) {
        this.idEnterprise = idEnterprise;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", password='" + password + '\'' +
                ", roles='" + roles + '\'' +
                ", active=" + active +
                ", salt='" + salt + '\'' +
                ", createdOn='" + createdOn + '\'' +
                ", lastLogin='" + lastLogin + '\'' +
                ", locale='" + locale + '\'' +
                ", idEnterprise='" + idEnterprise + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
