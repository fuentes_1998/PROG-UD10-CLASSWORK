package com.prog.activitat8;

import com.prog.activitat3.Validator;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class UserFactory {

    private Scanner sc;

    private Validator validator;

    private Random rnd;

    private String characters;

    public UserFactory() {

        this.sc = new Scanner(System.in);
        this.validator = new Validator();
        this.rnd = new Random();
        this.characters = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz";
    }

    public User create() {

        String firstName = this.getFirstName();

        String lastName = this.getLastName();

        String email = this.getEmail();

        String phoneNumber = this.getPhoneNumber();

        String password = this.getPassword();

        String locale = this.getLocale();

        int idEnterprise = this.getIdEnterprise();

        LocalDate birthday = this.getBirthday();

        return new User(firstName,lastName,email,phoneNumber,password,locale,idEnterprise,birthday);
    }

    private String getFirstName() {

        String firstName = "";

        int randomLenght = this.rnd.nextInt(30-1+1)+1;

        for (int i = 0; i <= randomLenght; i++) {

            firstName += this.characters.charAt(this.rnd.nextInt((this.characters.length()-1)));
        }

        return firstName;
    }

    private String getLastName() {

        String lastName = "";

        int randomLenght = this.rnd.nextInt(30-1+1)+1;

        for (int i = 0; i <= randomLenght; i++) {

            lastName += this.characters.charAt(this.rnd.nextInt((this.characters.length()-1)));
        }

        return lastName;
    }

    private String getEmail() {

        String email = "";

        int randomLenght = this.rnd.nextInt(30-1+1)+1;

        for (int i = 0; i <= randomLenght; i++) {

            email += this.characters.charAt(this.rnd.nextInt((this.characters.length()-1)));
        }

        return  email;
    }

    private String getPhoneNumber() {

        String phoneNumber = "6";

        do {

            phoneNumber += this.rnd.nextInt(9-1+1)+1;

        } while (phoneNumber.length() != 9);

        return phoneNumber;
    }

    private String getPassword() {

        String password = "";

        int randomLenght = this.rnd.nextInt(100-1+1)+1;

        for (int i = 0; i <= randomLenght; i++) {

            password += this.characters.charAt(this.rnd.nextInt((this.characters.length()-1)));
        }

        return password;
    }

    private String getLocale() {

        return "es_ES";
    }

    private int getIdEnterprise() {

        return this.rnd.nextInt(5-1+1)+1;
    }

    private LocalDate getBirthday() {

        System.out.println("Introduce el año en el que naciste");

        int year = this.sc.nextInt();

        System.out.println("Introduce el mes en el que naciste");

        int month = this.sc.nextInt();

        System.out.println("Introduce el dia en el que naciste");

        int day = this.sc.nextInt();

        return LocalDate.of(year,month,day);
    }

    private String getAnswer(String question){

        System.out.println(question);
        return this.sc.nextLine();

    }
}
