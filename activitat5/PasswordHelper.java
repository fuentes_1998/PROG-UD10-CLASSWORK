package com.prog.activitat5;

import org.apache.commons.codec.digest.DigestUtils;

public class PasswordHelper {

    PasswordHelper(){
        new PasswordHelper();
    }

    public static String getSha1(String password){

        return DigestUtils.sha1Hex(password);
    }

    public static boolean verifySHA(String password, String hash){

        return getSha1(password).equals(hash);
    }

}
