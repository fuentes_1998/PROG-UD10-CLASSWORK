package com.prog.activitat5;

import com.prog.activitat1.MySqlConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class App {

    public static void main( String[] args ) {

        MySqlConnection mySqlConnection = new MySqlConnection("crm_db", "root", "peripoll9");

        editAllUsersPassword(mySqlConnection);
    }


    private static void editAllUsersPassword(MySqlConnection mySQLConnection){

        Connection connection = mySQLConnection.getConnection();

        String query = "SELECT id, firstName, lastName, password FROM User";

        try {

            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {

                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String concatenatedName = firstName+"_"+lastName;

                rs.updateString("password", PasswordHelper.getSha1(concatenatedName));
                rs.updateRow();
            }

            connection.close();


        } catch (SQLException e){

            e.printStackTrace();

        }

    }

}


