package com.prog.activitat1;

import java.sql.*;

public class App {

    public static void main( String[] args ) throws SQLException {

        MySqlConnection mySqlConnection = new MySqlConnection( "crm_db", "user_java", "password_java");

        Connection connection = mySqlConnection.getConnection();

        if (connection.isValid(1000)){

            System.out.println("Conexion establecida correctamente");

        }

    }
}
