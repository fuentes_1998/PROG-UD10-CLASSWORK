package com.prog.activitat1;

import java.sql.*;

public class MySqlConnection {

    private String ip;

    private String db;

    private String user;

    private String password;

    private Connection connection;

    public MySqlConnection(String ip, String db, String user, String password) {

        this.ip = ip;
        this.db = db;
        this.user = user;
        this.password = password;

    }

    public MySqlConnection(String db, String user, String password) {

        this.ip = "localhost";
        this.db = db;
        this.user = user;
        this.password = password;

    }

    public Connection getConnection() {

        if (this.connection == null) {

            this.connect();

        }

        return connection;

    }

    private void connect () {

        try {

            String urlDb = this.getUrlConnection();
            this.connection = DriverManager.getConnection(urlDb, this.user, this.password);

        } catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();

        }

    }

    private String getUrlConnection(){

        return "jdbc:mysql://"+this.ip+"/"+this.db+"?serverTimezone=UTC&useSSL=false";

    }


}

