package com.prog.activitat2;

import com.prog.activitat1.MySqlConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Activitat2 {

    public static void main( String[] args ) {

        showUsersAndEnterprises();

    }

    private static void showUsersAndEnterprises(){

        MySqlConnection mySqlConnection = new MySqlConnection( "crm_db", "user_java", "password_java");

        String sql = "SELECT u.*, e.name FROM User AS u JOIN Enterprise AS e ON u.idEnterprise = e.id ";
        sql += "WHERE idEnterprise IN (1,2) ";

        Connection connection = mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {

                System.out.println("------------------------");
                System.out.println("FirstName: " +rs.getString("firstName"));
                System.out.println("LastName: " +rs.getString("lastName"));
                System.out.println("Empresa: " +rs.getString("name"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
