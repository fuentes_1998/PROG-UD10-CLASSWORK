package com.prog.activitat3;

public class InvalidInputException extends RuntimeException {

    InvalidInputException(String msg){

        super(msg);

    }
}
