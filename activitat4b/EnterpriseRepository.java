package com.prog.activitat4b;

import com.prog.activitat3.Enterprise;

public interface EnterpriseRepository {

     boolean save(Enterprise enterprise);

     Enterprise findById(int enterpriseId);

     Enterprise getById(int enterpriseId) throws EnterpriseNotFoundException;

}
