package com.prog.activitat4b;

import com.prog.activitat1.MySqlConnection;
import com.prog.activitat3.Enterprise;

import java.sql.*;

public class DbEnterpriseRepository implements EnterpriseRepository {

    private MySqlConnection mySqlConnection;

    public DbEnterpriseRepository(){

        this.mySqlConnection = new MySqlConnection("crm_db", "user_java", "password_java");

    }

    @Override
    public boolean save(Enterprise enterprise) {

        if (enterprise.getId() != null) {
            return this.update(enterprise);
        }

        return this.add(enterprise);

    }

    @Override
    public Enterprise findById(int enterpriseId) {

        String sql = "SELECT * FROM Enterprise WHERE id = "+enterpriseId;
        Connection connection = this.mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            if (rs.next()) {
                return new Enterprise(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("address"),
                        rs.getString("city"),
                        rs.getString("province"),
                        rs.getString("country"),
                        rs.getString("locale"),
                        rs.getString("nif"),
                        rs.getTimestamp("createdOn").toLocalDateTime(),
                        (rs.getInt("status") == 1)
                );
            }

            return null;

        } catch (SQLException e) {

            e.printStackTrace();
            return null;

        }

    }

    @Override
    public Enterprise getById(int enterpriseId) throws EnterpriseNotFoundException {

        Enterprise enterprise = findById(enterpriseId);

        if (enterprise != null) {
            return enterprise;
        }

        throw new EnterpriseNotFoundException();
    }

    private boolean update(Enterprise enterprise) {

        String sql = "UPDATE Enterprise SET ";
        sql+=   "name = '"+enterprise.getName()+"'," +
                "address = '"+enterprise.getAddress()+"'," +
                "city = '"+enterprise.getCity()+"'," +
                "province = '"+enterprise.getProvince()+"'," +
                "country = '"+enterprise.getCountry()+"'," +
                "locale = '"+enterprise.getLocale()+"'," +
                "nif = '"+enterprise.getNif()+"'," +
                "status = "+((enterprise.isActive()) ? 1 : 0) +
                " WHERE id = "+enterprise.getId();

        Connection connection = this.mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            return true;

        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        }

    }

    private boolean add(Enterprise enterprise) {

        String sql = "INSERT INTO Enterprise (name,address,city,province,country,locale,createdOn,nif,status) ";
        sql+="VALUES (" +
                "'"+enterprise.getName()+"'," +
                "'"+enterprise.getAddress()+"'," +
                "'"+enterprise.getCity()+"'," +
                "'"+enterprise.getProvince()+"'," +
                "'"+enterprise.getCountry()+"'," +
                "'"+enterprise.getLocale()+"'," +
                "'"+enterprise.getCreatedOnISOString()+"'," +
                "'"+enterprise.getNif()+"'," +
                +((enterprise.isActive()) ? 1 : 0) +
                " )";

        Connection connection = this.mySqlConnection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                int idEnterprise = generatedKeys.getInt(1);
                enterprise.setId(idEnterprise);
                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return false;
    }

}
