package com.prog.activitat4b;

import com.prog.activitat3.Enterprise;

import java.util.Random;


public class Activitat4b {

    public static void main( String[] args ) {

        Random random = new Random();
        Enterprise enterpriseToInsert = new Enterprise(
                "Enterprise_random",
                "Paseo de gracia",
                "Gandia",
                "Valencia",
                "es",
                "ca_ES",
                random.nextInt(10000)+""
        );

        DbEnterpriseRepository dbEnterpriseRepository = new DbEnterpriseRepository();
        dbEnterpriseRepository.save(enterpriseToInsert);

        Enterprise enterprise = null;
        try {
            enterprise = dbEnterpriseRepository.getById(enterpriseToInsert.getId());
            System.out.println(enterprise);
        } catch (EnterpriseNotFoundException e) {
            e.printStackTrace();
        }

        enterprise.setLocale("ca_ES");
        enterprise.setStatus(false);
        dbEnterpriseRepository.save(enterprise);
        System.out.println(dbEnterpriseRepository.findById(enterprise.getId()));

        enterprise.setLocale("es_ES");
        enterprise.setStatus(true);
        dbEnterpriseRepository.save(enterprise);
        System.out.println(dbEnterpriseRepository.findById(enterprise.getId()));

    }



}
